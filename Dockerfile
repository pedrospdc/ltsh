FROM alpine:3.9

WORKDIR /app

COPY tmp/ltsh /app/ltsh
EXPOSE 8080
CMD /app/ltsh