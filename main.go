package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pedrospdc/ltsh/views"
)

func main() {
	r := gin.Default()
	r.GET("/", views.Index)
	r.Run()
}
