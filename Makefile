DOCKER_IMAGE ?= "ltsh:latest"
export GOOS ?= linux
export GOARCH ?= amd64

tmp:
	mkdir tmp

build:
	go build -o tmp/ltsh

docker-build: build
	docker build . -t ${DOCKER_IMAGE}

.PHONY: \
	build \
	docker-build